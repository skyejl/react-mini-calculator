import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 0
        };
        this.onPlusOne = this.onPlusOne.bind(this);
        this.onMinusOne = this.onMinusOne.bind(this);
        this.onMultipleTwo = this.onMultipleTwo.bind(this);
        this.onDiviseTwo = this.onDiviseTwo.bind(this);
    }
    onPlusOne() {
        this.setState({
            value: ++this.state.value
        });
    }
    onMinusOne() {
        this.setState({
            value: --this.state.value
        });
    }
    onMultipleTwo(){
        this.setState({
            value: this.state.value * 2
        });
    }
    onDiviseTwo() {
        this.setState({
            value: this.state.value / 2
        });
    }

    render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.value} </span>
        </div>
        <div className="operations">
          <button onClick={this.onPlusOne}>加1</button>
          <button onClick={this.onMinusOne}>减1</button>
          <button onClick={this.onMultipleTwo}>乘以2</button>
          <button onClick={this.onDiviseTwo}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

